package com.example.errorhandlingapplication

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import kotlinx.android.synthetic.main.activity_validation_successful.*

class ValidationSuccessfulActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validation_successful)
        val mAwesomeValidation = AwesomeValidation(ValidationStyle.BASIC)
        btnSignup.setOnClickListener {

            mAwesomeValidation.addValidation(
                this,
                R.id.edtfirstName,
                "[a-zA-Z\\s]+",
                R.string.err_name
            );
            mAwesomeValidation.addValidation(
                this,
                R.id.edtlastName,
                "[a-zA-Z\\s]+",
                R.string.err_name
            );
            mAwesomeValidation.addValidation(
                this,
                R.id.edtNumber,
                "(^\\+\\d+)?[0-9\\s()-]*",
                R.string.err_tel
            );


            mAwesomeValidation.addValidation(
                this,
                R.id.edtEmail,
                Patterns.EMAIL_ADDRESS,
                R.string.err_email
            );

            val regexPassword =
                "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{8,}"
            mAwesomeValidation.addValidation(
                this,
                R.id.edtPassword,
                regexPassword,
                R.string.err_password
            );
            mAwesomeValidation.addValidation(
                this,
                R.id.edtConfPassword,
                R.id.edtPassword,
                R.string.err_password_confirmation
            )
            if (mAwesomeValidation.validate()){
                startActivity(Intent(this, MainActivity::class.java))

            }

        }

    }
}