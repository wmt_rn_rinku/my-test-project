package com.example.errorhandlingapplication

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.util.Patterns.*
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        intiView()


    }

    private fun intiView() {

        btnSignup.setOnClickListener {

            val email = edtEmail.text.toString()
            val number = edtNumber.text.toString()
            val password = edtPassword.text.toString()
            val confPassword = edtConfPassword.text.toString()

            if (edtfirstName.text.toString().isEmpty() && edtlastName.text.toString()
                    .isEmpty() && edtPassword.text.toString().isEmpty() && edtEmail.text.toString()
                    .isEmpty() && edtNumber.text.toString().isEmpty()
            ) {
                edtfirstName.error = "This field Cant be Empty"
                edtlastName.error = "This field Cant be Empty"
                edtEmail.error = "This field Cant be Empty"
                edtNumber.error = "This field Cant be Empty"
                edtPassword.error = "This field Cant be Empty"

            } else if (!EMAIL_ADDRESS.matcher(email).matches()) {
                edtEmail.error = "Please Enter Valid Email"
            }  else if (edtNumber.text.toString().length < 10) {
                edtNumber.error = "Please enter 10 Digit Number"
            } else if (!password.equals(confPassword)) {
                edtConfPassword.error = "Password Does not Match"
            } else {

                val i = Intent(this, ValidationSuccessfulActivity::class.java)
                startActivity(i)
            }


        }

    }

    override fun onBackPressed() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Warning");
        alertDialogBuilder.setMessage("Are you sure you Want to exit??");
        alertDialogBuilder.setPositiveButton(
            "No",
            DialogInterface.OnClickListener { dialog, which ->

                Toast.makeText(this, "Data Added Sucess fully", Toast.LENGTH_SHORT).show()
            })
        alertDialogBuilder.setNegativeButton(
            "Yes",
            DialogInterface.OnClickListener { dialog, which ->
                finish()

            })
        alertDialogBuilder.create()
        alertDialogBuilder.setCancelable(true)
        alertDialogBuilder.show()
    }

}


